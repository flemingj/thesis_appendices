################################################################################
#
# RevBayes Example: Total-evidence dating under the fossilized birth-death model
# 
# This file: Runs the full MCMC ...
#
# authors: Tracy A. Heath, Walker C. Pett, April M. Wright
#
################################################################################

#######################
# Reading in the Data #
#######################
# Create the full list of taxa (including all fossils and extant species #
taxa <- readTaxonData("Ecdyso_species.tsv")

# Import the molecular sequences #
# this file contains only the taxa for which sequence data are available #
cytb <- readDiscreteCharacterData("OrtegaData.nex")

# Import the morphological character matrix #
# this file contains only the taxa for which morphological characters are available #
morpho <- readDiscreteCharacterData("Ortega_morphology.nex")

# Add the missing taxa to each data partition #
cytb.addMissingTaxa( taxa )
morpho.addMissingTaxa( taxa )

## helpers
n_taxa <- taxa.size()

mvi = 1

# Load the model files

source("scripts/model_FBDRP_TEFBD.Rev") # FBD tree prior

source("scripts/model_UExp_TEFBD.Rev") # UExp relaxed clock

source("scripts/model_GTRG_TEFBD.Rev") # Molecular substitution model (GTR+G)

source("scripts/model_Morph_TEFBD.Rev") # Morphological character change model


########
# MCMC #
########

# initialize the model object #
mymodel = model(sf)

mni = 1

# Create a vector of monitors #
# 1. for the full model #
monitors[mni++] = mnModel(filename="output/OlderEcdysoMax.log", printgen=10)

# 2. the tree #
monitors[mni++] = mnFile(filename="output/OlderEcdysoMax.trees", printgen=10, fbd_tree)

# 3. and a few select parameters to be printed to the screen #
monitors[mni++] = mnScreen(printgen=10, num_samp_anc, origin_time)

# Initialize the MCMC object #
mymcmc = mcmc(mymodel, monitors, moves)

# Run the MCMC #
mymcmc.run(generations=1000000)

# Quit RevBayes #
q()
