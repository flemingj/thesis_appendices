##########################################################################################
# Joint Fossilized Birth-Death Process prior on the topology and fossil occurrence times #
##########################################################################################
Imp_Tree <- readTrees("Starter.tre")[1]

# Define exponential priors on the birth rate and death rate #
speciation_rate ~ dnExponential(10)
extinction_rate ~ dnExponential(10)

# Specify a scale move on the speciation_rate parameter #
# This move will be applied with 3 different tuning values (lambda) to help improve mixing # 
moves[mvi++] = mvScale(speciation_rate, lambda=0.01, weight=1)
moves[mvi++] = mvScale(speciation_rate, lambda=0.1,  weight=1)
moves[mvi++] = mvScale(speciation_rate, lambda=1.0,  weight=1)

# Specify a sliding-window move on the extinction_rate parameter #
# This move will be applied with 3 different window widths (delta) to help improve mixing # 
moves[mvi++] = mvScale(extinction_rate, lambda=0.01, weight=1)
moves[mvi++] = mvScale(extinction_rate, lambda=0.1,  weight=1)
moves[mvi++] = mvScale(extinction_rate, lambda=1,    weight=1)

# Create deterministic nodes for the diversification and turnover rates so that they can be monitored #
diversification := speciation_rate - extinction_rate
turnover := extinction_rate/speciation_rate

# Fix the probability of sampling parameter (rho) to 1, #
# because all extant bears are represented in this analysis #
rho <- 0.00001

# Assume an exponential prior on the rate of sampling fossils (psi) #
psi ~ dnExponential(10) 

# Specify a scale move on the psi parameter #
# This move will be applied with 3 different tuning values (lambda) to help improve mixing # 
moves[mvi++] = mvScale(psi, lambda=0.01, weight=1)
moves[mvi++] = mvScale(psi, lambda=0.1,  weight=1)
moves[mvi++] = mvScale(psi, lambda=1,    weight=1)

# The FBD is conditioned on a starting time for the process, which is the origin time #
# Specify a uniform prior on the origin #
origin_time ~ dnUnif(542, 636)

# Specify a sliding-window move on the origin_time parameter #
# This move will be applied with 3 different window widths (delta) to help improve mixing # 
moves[mvi++] = mvSlide(origin_time, delta=0.01, weight=5.0)
moves[mvi++] = mvSlide(origin_time, delta=0.1,  weight=5.0)
moves[mvi++] = mvSlide(origin_time, delta=1,    weight=5.0)


### Define the tree-prior distribution as the fossilized birth-death process ###
fbd_dist = dnFBDP(origin=origin_time, lambda=speciation_rate, mu=extinction_rate, psi=psi, rho=rho, taxa=taxa)


# The will be a random variable of a constrained topology distribution that is governed by the FBD #
# this distribution will generate FBD trees that match the monophyly constraints defined above #
clade_Ku=clade("Kuamaia_lata","Misszhouia_longicaudata")
clade_Lim=clade(clade_Ku,"Limulus_polyphemus")
clade_Arth=clade(clade_Lim,"Triops_cancriformis")
clade_Ala=clade("Alalcomenaeus_sp","Leanchoilia_superlata")
clade_AlaArth=clade(clade_Ala,clade_Arth)
clade_Fux=clade("Chengjiangocaris_kunmingensis","Fuxianhuia_xiaoshibaensis")
clade_Comp=clade(clade_Fux,clade_AlaArth)
clade_Pey=clade("Aegirocassis_benmoulae","Peytoia_nathorsti")
clade_Hurd=clade(clade_Pey,"Hurdia_victoria")
clade_Schinder=clade(clade_Hurd,"Schinderhannes_bartlesi")
clade_Lyra=clade(clade_Schinder,"Lyrarapax_unguispinus")
clade_Anomalo=clade(clade_Lyra,"Anomalocaris_canadensis")
clade_Facet=clade(clade_Anomalo,clade_Comp)
clade_Opa=clade(clade_Facet,"Opabinia_regalis")
clade_Pambd=clade(clade_Opa,"Pambdelurion_whittingtoni")
clade_Keryg=clade(clade_Pambd, "Kerygmachela_kierkegaardi")
clade_Hadra=clade(clade_Keryg, "Hadranax_augustus")
clade_Echi=clade("Echiniscus_testudo_Heterotardigrada","Actinarctus_doryphorus_Heterotardigrada")
clade_Hetero=clade(clade_Echi, "Batillipes_pennaki_Heterotardigrada")
clade_Eu=clade("Hypsibius_dujardini_Eutardigrada","Macrobiotus_cf_harmsworthi_Eutardigrada")
clade_ExtTardi=clade(clade_Eu,clade_Hetero)
clade_Tardi=clade(clade_ExtTardi,"Siberian_Orsten_tardigrade")
clade_ArthTardi=clade(clade_Hadra,clade_Tardi)
clade_Epi=clade("Metaperipatus_blainvillei_Peripatopsidae","Epiperipatus_biolleyi_Peripatidae")
clade_Euperi=clade(clade_Epi, "Euperipatoides_Peripatopsidae")
clade_Tertia=clade(clade_Euperi,"Tertiapatus_dominicanus")
clade_Antenna=clade(clade_Tertia,"Antennacanthopodia_gracilis")
clade_Orsten=clade(clade_Antenna,"Orstenotubulus_evamuellerae")
clade_Sparsa=clade(clade_Orsten,"Hallucigenia_sparsa")
clade_Fortis=clade(clade_Sparsa,"Hallucigenia_fortis")
clade_Monster=clade("Collins_monster_Burgess_Shale","Collins_monster_Emu_Bay")
clade_Collins=clade(clade_Monster, "Collinsium_ciliosum")
clade_Luoli=clade(clade_Collins, "Luolishania_longicruris")
clade_Acino=clade(clade_Luoli, "Acinocricus_stichus")
clade_Halluci=clade(clade_Acino, "Hallucigenia_hongmeia")
clade_Onycho=clade(clade_Halluci, clade_Fortis)
clade_Cardio=clade(clade_Onycho, "Cardiodictyon_catenulum")
clade_Micro=clade(clade_Cardio, "Microdictyon_sinicum")
clade_Pauci=clade(clade_Micro, "Paucipodia_inermis")
clade_Dictyon=clade(clade_Pauci, "Onychodictyon_gracilis")
clade_Diania=clade(clade_Dictyon, "Diania_cactiformis")
clade_Xenusion=clade(clade_Diania, "Xenusion_auerswaldae")
clade_Lobo=clade(clade_Xenusion, "Onychodictyon_ferox")
clade_LoboArth=clade(clade_Lobo, clade_ArthTardi)
clade_Jian=clade("Jianshanopodia_decora","Megadictyon_haikouensis")
clade_Basal1=clade(clade_Jian,clade_LoboArth)
clade_Basal2=clade(clade_Basal1,"Aysheaia_pendunculata")
clade_Basal3=clade(clade_Basal2,"Cricocosmia_jinningensis")
#clade_Basal4=clade(clade_Basal3,"Tubiluchus_troglodytes_Priapulida")

constraints = v(clade_Ku, clade_Lim, clade_Arth, clade_Ala, clade_AlaArth, clade_Fux, clade_Comp, clade_Pey, clade_Hurd, clade_Schinder, clade_Lyra, clade_Anomalo, clade_Facet, clade_Opa, clade_Pambd, clade_Keryg, clade_Hadra, clade_Echi, clade_Hetero, clade_Eu, clade_ExtTardi, clade_Tardi, clade_ArthTardi, clade_Epi, clade_Euperi, clade_Tertia, clade_Antenna, clade_Orsten, clade_Sparsa, clade_Fortis, clade_Monster, clade_Collins, clade_Luoli, clade_Acino, clade_Halluci, clade_Onycho, clade_Cardio, clade_Micro, clade_Pauci, clade_Dictyon, clade_Diania, clade_Xenusion, clade_Lobo, clade_LoboArth, clade_Jian, clade_Basal1, clade_Basal2, clade_Basal3)
print ("echo")
fbd_tree ~ dnConstrainedTopology(fbd_dist, constraints=constraints)
print ("echo")
#fbd_tree ~ dnConstrainedTopology(treeDistribution=fbd_dist)
#print ("echo")
#fbd_tree.setValue(Imp_tree)

tLimulus <- -510
tmrca_Arth := tmrca(fbd_tree,clade_Arth)
n_TMRCA_Arth := -(tmrca_Arth)
stem_Arth_Fossil ~ dnExponential(lambda=0.0333, offset=n_TMRCA_Arth)
stem_Arth_Fossil.clamp(tLimulus)


# Specify moves on the tree and node times #
# These moves update the tree topology 

# These moves update the node ages #
# Because we are conditioning on the origin time, we must also sample the root node age #
moves[mvi++] = mvNodeTimeSlideUniform(fbd_tree, weight=40.0)
moves[mvi++] = mvRootTimeSlideUniform(fbd_tree, origin_time, weight=5.0)


### Create deterministic nodes to monitor various tree statistics ###
# Monitor the number of sampled ancestors in the FBD-tree #
num_samp_anc := fbd_tree.numSampledAncestors();

