
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                  ---  PhyML 20120412  ---                                             
                            http://www.atgc-montpellier.fr/phyml                                          
                         Copyright CNRS - Universite Montpellier II                                 
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			PDE.phy
. Data set: 				#1
. Tree topology search : 		NNIs
. Initial tree: 			BioNJ
. Model of amino acids substitution: 	WAG
. Number of taxa: 			24
. Log-likelihood: 			-9629.27827
. Unconstrained likelihood: 		-6305.02883
. Parsimony: 				1449
. Tree size: 				8.06742
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		1.041
. Proportion of invariant: 		0.000

. Run ID:				none
. Random seed:				1517412176
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h3m25s (205 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
